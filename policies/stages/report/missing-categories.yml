.item: &item
  item: |
    - [ ] #{full_resource_reference} {{title}} {{labels}}

# Each of these is based on a Ruby rule that gets the label names back for an issue
# (labels.map(&:name)), greps those for the label names (preceded by \A to indicate
# the start of the line), and then checks (using the size method) to see if
# none were returned. If none were returned, then that issue does not have any of
# those labels and needs to be looked at.

resource_rules:
  issues:
    rules:
      - name: "Find issues without category label for devops::deploy"
        limits:
          most_recent: 100
        conditions:
          state: opened
          labels:
            - "devops::deploy"
          ruby: |
            DevopsLabels::TRIAGE_MISSING_CATEGORIES_PROJECTS.include?(resource[:project_id]) &&
              resource[:issue_type] == 'issue' &&
              !has_category_label_for_current_stage?

        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            <<: *item
            title: "`devops::deploy` issues without a category label"
            summary: |

              The following issues have been identified in the devops::deploy stage that do not have a category
              label. These should be applied with urgency so that important issues are not being lost track of.

              {{items}}

              ---

              You are welcome to help [improve this report](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/missing-categories.yml).

              /due in 7 days
              /assign @nagyv-gitlab
              /label ~"triage report" ~"devops::deploy"

      - name: "Find issues without category label for devops::package"
        limits:
          most_recent: 100
        conditions:
          state: opened
          labels:
            - "devops::package"
          ruby: |
            DevopsLabels::TRIAGE_MISSING_CATEGORIES_PROJECTS.include?(resource[:project_id]) &&
              resource[:issue_type] == 'issue' &&
              !has_category_label_for_current_stage?
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            <<: *item
            title: "`devops::package` issues without a category label"
            summary: |

              The following issues have been identified in the devops::package stage that do not have a category
              label. These should be applied with urgency so that important issues are not being lost track of.

              {{items}}

              ---

              You are welcome to help [improve this report](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/missing-categories.yml).

              /due in 7 days
              /assign @trizzi
              /label ~"triage report" ~"devops::package"

      - name: "Find issues without category label for devops::verify"
        limits:
          most_recent: 100
        conditions:
          state: opened
          forbidden_labels:
            - "failure::flaky-test"
            - "failure::new"
          labels:
            - "devops::verify"
          ruby: |
            DevopsLabels::TRIAGE_MISSING_CATEGORIES_PROJECTS.include?(resource[:project_id]) &&
              resource[:issue_type] == 'issue' &&
              !has_category_label_for_current_stage?
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            <<: *item
            title: "`devops::verify` issues without a category label"
            summary: |

              The following issues have been identified in the devops::verify stage that do not have a category
              label. These should be applied with urgency so that important issues are not being lost track of.

              {{items}}

              ---

              You are welcome to help [improve this report](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_DEFAULT_BRANCH']}/policies/stages/report/missing-categories.yml).

              /due in 7 days
              /assign @jreporter @dhershkovitch @DarrenEastman @jocelynjane @gabrielengel_gl
              /label ~"triage report" ~"devops::verify"
