# frozen_string_literal: true

require_relative '../../triage'

module Triage
  module PipelineFailure
    class PipelineIncidentFinder
      DESCRIPTION = 'DESCRIPTION'

      attr_reader :incident_project_id, :pipeline_id

      def initialize(incident_project_id:, pipeline_id:)
        @incident_project_id = incident_project_id
        @pipeline_id = pipeline_id
      end

      def latest_incident
        @latest_incident ||= incidents.last
      end

      def incidents
        @incidents ||= Triage.api_client.issues(
          incident_project_id,
          { search: pipeline_id, in: DESCRIPTION, order_by: 'created_at', sort: 'desc', per_page: 1 }
        ) || []
      end
    end
  end
end
