# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative '../../../triage/resources/ci_job'
require_relative '../../../lib/constants/labels'
require_relative 'failure_trace'

module Triage
  module PipelineFailure
    class TriageIncident
      ROOT_CAUSE_LABELS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS
      LABELS_FOR_TRANSIENT_ERRORS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.values_at(
        :failed_to_pull_image,
        :gitlab_com_overloaded,
        :runner_disk_full,
        :infrastructure,
        :pg_query_canceled
      ).freeze

      POST_RETRY_JOB_URL_THRESHOLD = 10
      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human

      attr_reader :event, :config, :failed_jobs, :ci_jobs

      def initialize(event:, config:, failed_jobs:)
        @event = event
        @config = config
        @failed_jobs = failed_jobs
        @ci_jobs = failed_jobs.to_h { |job| [job.id, Triage::CiJob.new(event.project_id, job.id)] }
      end

      def top_root_cause_label
        return ROOT_CAUSE_LABELS[:default] if triaged_jobs.empty?

        # find the top root cause label preferably not master-broken::undetermined
        potential_root_cause_labels
          .tally
          .max_by { |label, count| label == ROOT_CAUSE_LABELS[:default] ? 0 : count }[0]
      end

      def top_group_label
        return if triaged_jobs.empty? || potential_group_labels.empty?

        potential_group_labels
          .tally
          .max_by { |_label, count| count }[0]
      end

      def root_cause_analysis_comment
        return if triaged_jobs.empty?

        [
          list_all_triaged_jobs_comment,
          retry_pipeline_comment,
          close_incident_comment
        ].compact.join("\n\n").prepend("\n\n")
      end

      def investigation_comment
        return if triaged_jobs.empty? || closeable?

        jobs_with_failed_tests = triaged_jobs.reject { |triaged_job| triaged_job[:failed_test_trace].nil? }
        return if jobs_with_failed_tests.empty?

        jobs_with_failed_tests.map do |triaged_job|
          <<~MARKDOWN.chomp
            - #{triaged_job[:link]}:

            #{triaged_job[:failed_test_trace]}
          MARKDOWN
        end.join("\n\n").prepend("\n\n")
      end

      def attribution_comment
        return if triaged_jobs.empty?

        triaged_jobs.map do |triaged_job|
          <<~MARKDOWN.chomp
            #{triaged_job[:attribution_message]}
          MARKDOWN
        end.uniq.reject(&:empty?).join("\n\n").prepend("\n\n")
      end

      private

      def list_all_triaged_jobs_comment
        return if triaged_jobs.empty?

        triaged_jobs.map do |triaged_job|
          %(- #{triaged_job[:link]}: ~"#{triaged_job[:label]}".#{retry_job_comment(triaged_job)})
        end.join("\n")
      end

      def triaged_jobs
        @triaged_jobs ||=
          failed_jobs.map do |job|
            failure_trace = FailureTrace.new(
              trace: ci_jobs.fetch(job.id).trace,
              failed_examples_data: ci_jobs.fetch(job.id).failed_rspec_test_metadata,
              config: config
            )

            {
              id: job.id,
              link: "[#{job.name}](#{job.web_url})",
              label: failure_trace.root_cause_label,
              failed_test_trace: failure_trace.summary_markdown,
              attribution_labels: failure_trace.potential_group_labels,
              attribution_message: failure_trace.attribution_message_markdown
            }
          end
      end

      def retry_job_comment(failed_job)
        retry_job_web_url = retry_job_and_return_web_url_if_applicable(failed_job)
        return if retry_job_web_url.nil?

        " Retried at: #{retry_job_web_url}"
      end

      def retry_job_and_return_web_url_if_applicable(triaged_job)
        return unless transient_error?(triaged_job[:label]) && retry_jobs_individually?

        ci_jobs.fetch(triaged_job[:id]).retry['web_url']
      end

      def transient_error?(root_cause_label)
        LABELS_FOR_TRANSIENT_ERRORS.include?(root_cause_label)
      end

      def retry_jobs_individually?
        # the benefit of retrying individual job is so we can post the retried job url
        # but if there are too many failed jobs
        # it's more practical to click `retry pipeline` and verify the overall pipeline status when all jobs finish
        failed_jobs.size < POST_RETRY_JOB_URL_THRESHOLD
      end

      def retry_pipeline_comment
        return unless all_jobs_failed_with_transient_errors?
        return if retry_jobs_individually?

        retry_pipeline_response = Triage.api_client.retry_pipeline(event.project_id, event.id).to_h

        "Retried pipeline: #{retry_pipeline_response['web_url']}"
      end

      def close_incident_comment
        return unless closeable?

        "This incident is caused by known transient error(s), closing."
      end

      def all_jobs_failed_with_transient_errors?
        return @all_jobs_failed_with_transient_errors if defined?(@all_jobs_failed_with_transient_errors)

        @all_jobs_failed_with_transient_errors =
          if triaged_jobs.empty?
            false
          else
            (potential_root_cause_labels.uniq - config.transient_root_cause_labels).empty?
          end
      end
      alias_method :closeable?, :all_jobs_failed_with_transient_errors?
      public :closeable?

      def potential_group_labels
        @potential_group_labels ||= (triaged_jobs.map { |job| job[:attribution_labels] }).flatten
      end

      def potential_root_cause_labels
        @potential_root_cause_labels ||= triaged_jobs.map { |job| job[:label] }
      end
    end
  end
end
