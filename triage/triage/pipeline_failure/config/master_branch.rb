# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class MasterBranch < Base
        INCIDENT_PROJECT_ID = '40549124' # gitlab-org/quality/engineering-productivity/master-broken-incidents
        INCIDENT_LABELS = ['Engineering Productivity'].freeze
        INCIDENT_TEMPLATE = <<~MARKDOWN
          ## %<project_link>s pipeline %<pipeline_link>s failed

          **Branch: %<branch_link>s**

          **Commit: %<commit_link>s**

          **Triggered by** %<triggered_by_link>s • **Source:** %<source>s • **Duration:** %<pipeline_duration>s minutes

          **Failed jobs (%<failed_jobs_count>s):**

          %<failed_jobs_list>s

          ### General guidelines

          Follow the [Broken `master` handbook guide](https://about.gitlab.com/handbook/engineering/workflow/#broken-master).

          %<attribution_body>s

          ### Investigation

          **Be sure to fill the `Timeline` for this incident.**

          1. If the failure is new, and looks like a potential flaky failure, you can retry the failing job.
            Make sure to mention the retry in the `Timeline` and leave a link to the retried job.
          1. If the failure looks like a broken `master`, communicate the broken `master` in Slack using the "Broadcast Master Broken" workflow:
            - Click the Shortcut lightning bolt icon in the `#master-broken` channel and select "Broadcast Master Broken".
            - Click "Continue the broadcast" after the automated message in `#master-broken`.

          ### Pre-resolution

          If you believe that there's an easy resolution by either:

          - Reverting a particular merge request.
          - Making a quick fix (for example, one line or a few similar simple changes in a few lines).
            You can create a merge request, assign to any available maintainer, and ping people that were involved/related to the introduction of the failure.
            Additionally, a message can be posted in `#backend_maintainers` or `#frontend_maintainers` to get a maintainer take a look at the fix ASAP.

          In both cases, make sure to add the ~"pipeline:expedite" label, and `master:broken` or `master:foss-broken` label, to speed up the `master`-fixing pipelines.

          ### Resolution

          Follow [the Resolution steps from the handbook](https://about.gitlab.com/handbook/engineering/workflow/#responsibilities-of-the-resolution-dri).
        MARKDOWN

        SLACK_CHANNEL = 'master-broken'

        def self.match?(event)
          event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            !event.merge_request_pipeline? &&
            event.ref == 'master' &&
            event.source_job_id.nil?
        end

        def incident_project_id
          INCIDENT_PROJECT_ID
        end

        def incident_template
          INCIDENT_TEMPLATE
        end

        def incident_labels
          master_broken_label =
            if event.project_path_with_namespace.end_with?('gitlab-foss')
              'master:foss-broken'
            else
              'master:broken'
            end

          [*INCIDENT_LABELS, master_broken_label]
        end

        def slack_channel
          SLACK_CHANNEL
        end

        def auto_triage?
          true
        end
      end
    end
  end
end
