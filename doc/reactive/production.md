# Production

- [A problem is happening in production](#a-problem-is-happening-in-production)
- [Deployment](#deployment)
  - [triage-ops](#triage-ops)
  - [triage-ops dependencies](#triage-ops-dependencies)
    - [External dependency](#external-dependency)
    - [Internal dependency](#internal-dependency)
- [Concepts](#concepts)
  - [Ingress](#ingress)
  - [Ingress controller](#ingress-controller)
- [Putting it all together: Where does the triage-ops traffic arrive to the Rack app?](#putting-it-all-together-where-does-the-triage-ops-traffic-arrive-to-the-rack-app)
    - [Setting up `kubectl` locally for production access](#setting-up-kubectl-locally-for-production-access)
- [Monitoring](#monitoring)

## A problem is happening in production

Please refer to the [triage-ops RUNBOOK](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/blob/main/runbooks/triage-ops.md#production-incidents).
## Deployment

### triage-ops

We deploy triage-ops to Kubernetes via the project's CI/CD pipeline (see the `deploy:triage-web` job in `.gitlab/ci/triage-web.yml`).

### triage-ops dependencies

We deploy the following dependencies for triage-ops to work properly:

#### External dependency

* cert-manager (for TLS certificate management)
* ingres-nginx (our ingress controller)

#### Internal dependency

* gitlab-agent (for GitLab to properly communicate with the k8s cluster)

We deploy the softwares above via Terraform in the `engineering-productivity-infrastructure` project: https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/blob/main/qa-resources/modules/triage-reactive/main.tf.

## Concepts

### Ingress

We deploy a Kubernetes Ingress resource (https://kubernetes.io/docs/concepts/services-networking/ingress/) for routing the traffic to the triage-ops pod (this is conceptually equivalent to the `routes.rb` in Rails):

```shell
$ kubectl describe ing triage-web-ingress
[...]
Rules:
  Host                   Path  Backends
  ----                   ----  --------
  triage-ops.gitlab.com
                         /             triage-web-service:80 (10.20.3.41:8080)
                         /dashboard/   triage-web-service:80 (10.20.3.41:8080)
```

### Ingress controller

An ingress resource does nothing on its own. The ingress controller (https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) is the one making the actual routing. We use [nginx-ingress-controller](https://docs.nginx.com/nginx-ingress-controller/) for that (GCP also has an ingress controller that could use).

When deploying this `nginx-ingress-controller` helm chart, it will deploy a k8s service of type `loadBalancer`, which will create an actual external load balancer in GCP with an IP.

## Putting it all together: Where does the triage-ops traffic arrive to the Rack app?

The [`triage-ops-prod` Kubernetes cluster](https://console.cloud.google.com/kubernetes/clusters/details/us-central1/triage-ops-prod/details?project=gitlab-qa-resources)
is in the project [gitlab-qa-resources](https://console.cloud.google.com/kubernetes/list?project=gitlab-qa-resources).

This is a Rack application which processes the webhooks.

To arrive to this Rack app, the traffic needs to go through a few steps:

- <https://triage-ops.gitlab.com/> Resolve [DNS records](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14764) to IP pointing to the `ingress-nginx-controller` Service:

```shell
kubectl -n ingress-nginx describe service ingress-nginx-controller
```

- Then, traffic goes to `triage-web-ingress` since it has the `kubernetes.io/ingress.class: "nginx"` annotation

```shell
kubectl describe ingress -l app=triage-web --all-namespaces
```

- Then, traffic goes to `triage-web-service` thanks to `rules[0].http.paths[0].backend.serviceName: triage-web-service`

```shell
kubectl describe service -l app=triage-web --all-namespaces
```

Note: Both `triage-web-ingress` and `triage-web-service` are part of `triage-web-deployment`:

```shell
kubectl describe deployment -l app=triage-web --all-namespaces
```
#### Setting up `kubectl` locally for production access

Follow steps from the [engineering productivity page](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/blob/main/local_setup.md).
## Monitoring

- We use a [GCP uptime check](https://console.cloud.google.com/monitoring/uptime/triage-ops-3G07V7ZWrr0?project=gitlab-qa-resources) configured to alert in Slack as soon as triage-ops isn't accessible. It is [configured in Terraform](https://gitlab.com/gitlab-org/quality/engineering-productivity-infrastructure/blob/main/qa-resources/modules/triage-reactive/main.tf#L92-117).
- [Triage reactive overview dashboard](https://console.cloud.google.com/monitoring/dashboards/builder/26e89be1-e184-434e-b125-fff8e60aeec2?project=gitlab-qa-resources&dashboardBuilderState=%257B%2522editModeEnabled%2522:false%257D&timeDomain=1w)
- [Logs explorer](https://cloudlogging.app.goo.gl/EUPCuUQFj6NdB5AY6)
- [Sentry project](https://sentry.gitlab.net/gitlab/triage-ops/)
