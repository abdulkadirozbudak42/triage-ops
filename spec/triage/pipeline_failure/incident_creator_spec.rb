# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/incident_creator'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'
require_relative '../../../triage/triage/pipeline_failure/config/stable_branch'

FIXTURE_PATH = '/reactive/job_traces'

RSpec.describe Triage::PipelineFailure::IncidentCreator do
  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:ref) { 'master' }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      id: 42,
      project_path_with_namespace: project_path_with_namespace,
      project_id: 999,
      ref: ref,
      sha: 'sha',
      source_job_id: nil,
      web_url: 'web_url',
      commit_header: 'commit_header',
      event_actor: Triage::User.new(id: '1', name: 'John Doe', username: 'john'),
      source: 'source',
      created_at: Time.now,
      merge_request: Triage::MergeRequest.new(title: 'Hello', author: { id: '1' }),
      project_web_url: "https://gitlab.test/#{project_path_with_namespace}")
  end

  let(:first_job_name) { 'foo' }
  let(:second_job_name) { 'a' }
  let(:third_job_name) { 'baz' }
  let(:failed_jobs) do
    [
      double(id: '1', name: first_job_name, web_url: 'foo_web_url', allow_failure: false),
      double(id: '2', name: second_job_name, web_url: 'foo_web_url', allow_failure: false),
      double(id: '3', name: third_job_name, web_url: 'baz_web_url', allow_failure: false)
    ]
  end

  let(:incident_project_id) { config.incident_project_id }
  let(:api_client_double)   { double('API client') }
  let(:incidents)           { [] }

  let(:job_artifact_file) { Gitlab::FileResponse.new(Tempfile.new("/reactive/job_traces/rspec.json")) }

  subject { described_class.new(event: event, config: config, failed_jobs: failed_jobs) }

  around do |example|
    Timecop.freeze(Time.utc(2020, 3, 31, 8)) { example.run }
  end

  before do
    allow(Triage).to receive(:api_client).and_return(api_client_double)
    allow(api_client_double).to receive(:issues).with(
      incident_project_id, { in: 'DESCRIPTION', search: event.id }
    ).and_return(incidents)
  end

  describe '#execute' do
    shared_examples 'incident creation' do
      let(:incident_double)     { double('Incident', iid: 4567) }
      let(:discussion_path)     { "/projects/#{incident_project_id}/issues/#{incident_double.iid}/discussions" }
      let(:job_trace) { 'ERROR: Job failed: failed to pull image "registry.gitlab.com"' }

      before do
        allow(api_client_double).to receive(:job_retry).and_return({ 'web_url' => 'retry_web_url' })
      end

      context 'when ref is master' do
        let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }

        before do
          allow(api_client_double).to receive(:job_trace).and_return(job_trace)
        end

        context 'with job trace showing transient failures' do
          before do
            allow(api_client_double).to receive(:download_job_artifact_file).and_return(job_artifact_file)
          end

          it 'creates an incident and triages root cause' do
            expect(Triage.api_client).to receive(:create_issue)
              .with(
                incident_project_id,
                expected_incident_title,
                {
                  issue_type: 'incident',
                  description: format(config.incident_template, subject.__send__(:template_variables)),
                  labels: [*config.incident_labels, 'master-broken::failed-to-pull-image']
                })
              .and_return(incident_double)

            expect(Triage.api_client).to receive(:post)
              .with(
                discussion_path,
                body: {
                  body: <<~MARKDOWN.chomp
                    ## Root Cause Analysis

                    - [#{first_job_name}](foo_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_web_url
                    - [#{second_job_name}](foo_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_web_url
                    - [#{third_job_name}](baz_web_url): ~"master-broken::failed-to-pull-image". Retried at: retry_web_url

                    This incident is caused by known transient error(s), closing.
                  MARKDOWN
                }
              )

            expect(Triage.api_client).to receive(:post)
              .with(
                discussion_path,
                body: { body: '## Investigation Steps' })

            expect(Triage.api_client).to receive(:close_issue).with(incident_project_id, incident_double.iid)

            subject.execute
          end
        end

        context 'with job trace showing RSpec test failures' do
          let(:job_trace) do
            <<~MARKDOWN.chomp
            Failures:
            test failure details

            [TEST PROF INFO]
            MARKDOWN
          end

          before do
            allow(api_client_double).to receive(:download_job_artifact_file).and_return(job_artifact_file)
            stub_request(:get, "https://about.gitlab.com/categories.json")
              .to_return(status: 200, body: read_fixture('categories.json'), headers: {})
            stub_request(:get, "https://about.gitlab.com/groups.json")
              .to_return(status: 200, body: read_fixture('groups.json'), headers: {})

            job_artifact_file.write(read_fixture('/reactive/job_traces/rspec.json'))
            job_artifact_file.rewind
          end

          after do
            job_artifact_file.unlink
          end

          it 'creates an incident and posts relevant job trace to investigation steps' do
            expect(Triage.api_client).to receive(:create_issue)
              .with(
                incident_project_id,
                expected_incident_title,
                {
                  issue_type: 'incident',
                  description: format(config.incident_template, subject.__send__(:template_variables)),
                  labels: [*config.incident_labels, 'master-broken::undetermined', 'group::project management']
                })
              .and_return(incident_double)

            expect(Triage.api_client).to receive(:post)
              .with(
                discussion_path,
                body: {
                  body: <<~MARKDOWN.chomp
                    ## Root Cause Analysis

                    - [#{first_job_name}](foo_web_url): ~"master-broken::undetermined".
                    - [#{second_job_name}](foo_web_url): ~"master-broken::undetermined".
                    - [#{third_job_name}](baz_web_url): ~"master-broken::undetermined".
                  MARKDOWN
                }
              )

            expect(Triage.api_client).to receive(:post)
              .with(
                discussion_path,
                body: {
                  body: <<~MARKDOWN.chomp
                    ## Investigation Steps

                    - [#{first_job_name}](foo_web_url):

                    ```ruby
                    test failure details
                    ```

                    /label ~backend

                    - [#{second_job_name}](foo_web_url):

                    ```ruby
                    test failure details
                    ```

                    /label ~backend

                    - [#{third_job_name}](baz_web_url):

                    ```ruby
                    test failure details
                    ```

                    /label ~backend
                  MARKDOWN
                })

            subject.execute
          end
        end
      end

      context 'when ref is a stable branch' do
        let(:ref) { '7-11-stable-ee' }
        let(:config) { Triage::PipelineFailure::Config::StableBranch.new(event) }

        before do
          allow(api_client_double).to receive(:job_trace).and_return(job_trace)
          allow(api_client_double).to receive(:download_job_artifact_file).and_return(job_artifact_file)
        end

        it 'creates an incident without triage root cause' do
          expect(Triage.api_client).to receive(:create_issue).and_return(incident_double)
          expect(Triage.api_client).to receive(:post)
            .with(
              discussion_path,
              body: {
                body: <<~MARKDOWN.chomp
                  ## Root Cause Analysis
                MARKDOWN
              }
            )

          expect(Triage.api_client).to receive(:post)
            .with(
              discussion_path,
              body: { body: '## Investigation Steps' })

          subject.execute
        end
      end
    end

    it_behaves_like 'incident creation' do
      let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `#{project_path_with_namespace}` broken `#{ref}` with #{first_job_name}, #{second_job_name}, #{third_job_name}" }
    end

    context 'with a long job name' do
      let(:second_job_name) { 'a' * 175 }

      it_behaves_like 'incident creation' do
        let(:expected_incident_title) { "Tuesday 2020-03-31 08:00 UTC - `#{project_path_with_namespace}` broken `#{ref}` with #{first_job_name}, #{second_job_name}..." }
      end
    end
  end
end
