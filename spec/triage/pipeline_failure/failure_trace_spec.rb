# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage/event'
require_relative '../../../triage/triage/pipeline_failure/failure_trace'
require_relative '../../../triage/triage/pipeline_failure/config/master_branch'

GITLAB_PROJECT_ID = Triage::Event::GITLAB_PROJECT_ID
FIXTURE_PATH      = '/reactive/job_traces'

RSpec.describe Triage::PipelineFailure::FailureTrace do
  let(:event) { instance_double(Triage::PipelineEvent, id: 123, project_id: GITLAB_PROJECT_ID) }
  let(:config) { Triage::PipelineFailure::Config::MasterBranch.new(event) }
  let(:failed_examples_data) do
    [
      { "id" => "./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]", "status" => "failed", "feature_category" => "team_planning", "product_group_label" => "group::project management", "feature_category_label" => "Category:Team Planning" },
      { "id" => "./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:4]", "status" => "failed", "feature_category" => "team_planning", "product_group_label" => "group::project management", "feature_category_label" => "Category:Team Planning" }
    ]
  end

  let(:backend_trace) { read_fixture("#{FIXTURE_PATH}/rspec_failure.txt") }
  let(:frontend_trace) { read_fixture("#{FIXTURE_PATH}/jest_failure.txt") }

  it 'adds ~frontend label for frontend trace' do
    failure_trace = described_class.new(trace: frontend_trace, config: config, failed_examples_data: failed_examples_data)

    expect(failure_trace.summary_markdown).to eq(
      <<~MARKDOWN.chomp
      ```javascript
      FAIL spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js
        ● vue_shared/component/markdown/markdown_editor › disabled › disables content editor when disabled prop is true

          : Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Timeout - Async callback was not invoked within the 5000 ms timeout specified by jest.setTimeout.Error:

            148 |     });
            149 |
          > 150 |     it('disables content editor when disabled prop is true', async () => {
                |     ^
            151 |       buildWrapper({ propsData: { disabled: true } });
            152 |
            153 |       await enableContentEditor();

            at new Spec (node_modules/jest-jasmine2/build/jasmine/Spec.js:124:22)
            at Suite.it (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:150:5)
            at Suite.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:137:3)
            at Object.describe (spec/frontend/vue_shared/components/markdown/markdown_editor_spec.js:19:1)


      Test Suites: 1 failed, 532 passed, 533 total
      Tests:       1 failed, 2 skipped, 32 todo, 7782 passed, 7817 total
      Snapshots:   56 passed, 56 total
      Time:        1332.608 s
      ```

      /label ~frontend
      MARKDOWN
    )
  end

  it 'adds ~backend label for backend trace' do
    failure_trace = described_class.new(trace: backend_trace, config: config, failed_examples_data: failed_examples_data)

    expect(failure_trace.summary_markdown).to eq(
      <<~MARKDOWN.chomp
      ```ruby

        1) Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
           Got 0 failures and 2 other errors:
           Shared Example Group: "Set to auto-merge activator" called from ./spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb:161

           1.1) Failure/Error: super

                Capybara::ElementNotFound:
                  Unable to find button "Set to auto-merge" that is not disabled

                  Timeout (45s) reached while running a waiting Capybara finder.
                # ------------------
                # --- Caused by: ---
                # Capybara::ElementNotFound:
                #   Unable to find button "Set to auto-merge" that is not disabled
                #   ./spec/support/capybara_slow_finder.rb:18:in `synchronize'

      Finished in 2 minutes 6.7 seconds (files took 1 minute 2.1 seconds to load)
      1 example, 1 failure

      Failed examples:

      rspec './spec/features/merge_request/user_merges_when_pipeline_succeeds_spec.rb[1:2:1:3:1:1]' # Merge request > User merges when pipeline succeeds when there is active pipeline for merge request with auto_merge_labels_mr_widget on enabling Merge when pipeline succeeds when it was enabled and then canceled behaves like Set to auto-merge activator activates the Merge when pipeline succeeds feature
      ```

      /label ~backend
      MARKDOWN
    )
  end

  describe 'attribution message' do
    context 'with valid feature_categories' do
      it 'builds the correct attribution message' do
        failure_trace = described_class.new(trace: backend_trace, config: config, failed_examples_data: failed_examples_data)

        expect(failure_trace.attribution_message_markdown).to eq(
          <<~MARKDOWN.chomp
            - ~"group::project management" ~"Category:Team Planning" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]
            - ~"group::project management" ~"Category:Team Planning" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:4]
          MARKDOWN
        )
      end
    end

    context 'with missing feature category' do
      let(:failed_examples_data) do
        [
          { "id" => "./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]", "status" => "failed", "feature_category" => "team_planning", "product_group_label" => DevopsLabels::MISSING_PRODUCT_GROUP_LABEL, "feature_category_label" => DevopsLabels::MISSING_FEATURE_CATEGORY_LABEL }
        ]
      end

      it 'builds thecorrect attribution message' do
        failure_trace = described_class.new(trace: backend_trace, config: config, failed_examples_data: failed_examples_data)

        expect(failure_trace.attribution_message_markdown).to eq(
          <<~MARKDOWN.chomp
            - ~"missing product_group_label" ~"missing feature_category label" ./spec/features/boards/sidebar_spec.rb[1:1:8:1:1:3]
          MARKDOWN
        )
      end
    end
  end
end
