# frozen_string_literal: true

RSpec.shared_context 'with community contribution processor' do
  let(:wider_community_contribution) { true }

  before do
    allow(subject).to receive(:wider_community_contribution?).and_return(wider_community_contribution)
    allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(true)
  end
end

RSpec.shared_examples 'community contribution processor #applicable?' do
  include_context 'with community contribution processor'

  include_examples 'event is applicable'

  context 'when event is not for a wider community contribution' do
    let(:wider_community_contribution) { false }

    include_examples 'event is not applicable'
  end
end

RSpec.shared_context 'when command is a valid command from a wider community contribution' do
  before do
    allow(subject).to receive(:valid_command?).and_return(true)
  end
end

RSpec.shared_examples 'community contribution command processor #applicable?' do
  include_context 'when command is a valid command from a wider community contribution'
  include_examples 'event is applicable'

  context 'when command is not a valid command from a wider community contribution' do
    before do
      allow(subject).to receive(:valid_command?).and_return(false)
    end

    include_examples 'event is not applicable'
  end
end

RSpec.shared_context 'with community contribution open resource' do
  let(:wider_community_contribution_open_resource) { true }

  before do
    allow(subject).to receive(:wider_community_contribution_open_resource?).and_return(wider_community_contribution_open_resource)
    allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(true)
  end
end

RSpec.shared_examples 'community contribution open resource #applicable?' do
  include_context 'with community contribution open resource'
  include_examples 'event is applicable'

  context 'when event project is not under gitlab-org, or author is not from the wider community, or resource is not open' do
    let(:wider_community_contribution_open_resource) { false }

    include_examples 'event is not applicable'
  end
end
